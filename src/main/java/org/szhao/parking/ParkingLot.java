package org.szhao.parking;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

/**
 * @author zsh
 * created 2020/Dec/05
 */

@Getter
@EqualsAndHashCode
@ToString
public class ParkingLot {
    private final int id;
    private final ChargerType chargerType;

    public ParkingLot(int id, ChargerType chargerType) {
        this.id = id;
        this.chargerType = chargerType;
    }
}
