package org.szhao.parking.api;


import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.NativeWebRequest;
import org.szhao.codegen.api.ParkApi;
import org.szhao.codegen.api.UnparkApi;
import org.szhao.parking.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * @author zsh
 * created 2020/Dec/06
 */
@RestController
public class ParkingController implements ParkApi, UnparkApi {

    private final Parking parking;
    public ParkingController() {
        PricingPolicy policy = new PerHourWithFixedAmount(1.5, 2.0);
        Map<ChargerType, Integer> map = new HashMap<>();
        map.put(ChargerType.NONE, 2);
        map.put(ChargerType.KW20, 3);
        map.put(ChargerType.KW50, 5);

        parking = new Parking(policy, map);
    }
    @Override
    public Optional<NativeWebRequest> getRequest() {
        return Optional.empty();
    }

    @Override
    public ResponseEntity<Ticket> park(@Valid Vehicle vehicle) {
        Optional<Ticket> t = parking.park(vehicle);
        if (t.isEmpty()) {
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(t.get());
    }

    @Override
    public ResponseEntity<Ticket> unpark(@Valid Vehicle vehicle) {
        Optional<Ticket> t = parking.unpark(vehicle);
        if (t.isEmpty()) {
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(t.get());
    }
}
