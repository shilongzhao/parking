package org.szhao.parking;

import lombok.Getter;
import lombok.ToString;

import java.time.Duration;

/**
 * @author zsh
 * created 2020/Dec/05
 */
@Getter @ToString
public class PerHour implements PricingPolicy {

    private final double pricePerHour;

    public PerHour(double pricePerHour) {
        this.pricePerHour = pricePerHour;
    }

    @Override
    public double getPrice(Duration duration) {
        return pricePerHour * duration.getSeconds()/ (60 * 60.0);
    }
}
