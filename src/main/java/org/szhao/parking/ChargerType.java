package org.szhao.parking;

/**
 * @author zsh
 * created 2020/Dec/05
 */
public enum ChargerType {
    KW20,
    KW50,
    NONE
}
