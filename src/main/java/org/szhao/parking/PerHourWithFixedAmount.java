package org.szhao.parking;

import lombok.Getter;
import lombok.ToString;

import java.time.Duration;

/**
 * @author zsh
 * created 2020/Dec/05
 */
@Getter @ToString
public class PerHourWithFixedAmount implements PricingPolicy {
    private final double fixedAmount;
    private final double pricePerHour;

    public PerHourWithFixedAmount(double fixedAmount, double pricePerHour) {
        this.fixedAmount = fixedAmount;
        this.pricePerHour = pricePerHour;
    }

    @Override
    public double getPrice(Duration duration) {
        return fixedAmount + duration.getSeconds()/(60.0 * 60) * pricePerHour;
    }
}
