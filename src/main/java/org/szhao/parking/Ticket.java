package org.szhao.parking;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.ToString;

import java.time.Duration;
import java.time.LocalDateTime;

/**
 * @author zsh
 * created 2020/Dec/05
 */
@Getter
@ToString
public class Ticket {
    private final Vehicle vehicle;
    private final ParkingLot parkingLot;
    private final LocalDateTime start;
    private LocalDateTime end;
    private final PricingPolicy policy;
    private double price;

    public Ticket(Vehicle vehicle, ParkingLot parkingLot, LocalDateTime start, PricingPolicy policy) {
        this.vehicle = vehicle;
        this.parkingLot = parkingLot;
        this.start = start;
        this.policy = policy;
    }

    public void setEnd(LocalDateTime end) {
        this.end = end;
        Duration d = Duration.between(start, end);
        this.price = policy.getPrice(d);
    }
}
