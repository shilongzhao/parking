package org.szhao.parking;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

/**
 * @author zsh
 * created 2020/Dec/05
 */

@Getter
@JsonIgnoreProperties(ignoreUnknown = true)
@EqualsAndHashCode
@ToString
public class Vehicle {

    private final String plate;
    private final ChargerType chargerType;

    public Vehicle(String plate, ChargerType chargerType) {
        this.plate = plate;
        this.chargerType = chargerType;
    }

}
