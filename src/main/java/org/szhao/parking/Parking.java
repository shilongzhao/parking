package org.szhao.parking;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;
import java.util.*;

/**
 * @author zsh
 * created 2020/Dec/05
 */
public class Parking {
    private static final Logger log = LoggerFactory.getLogger(Parking.class);

    final private PricingPolicy policy;
    Map<Vehicle, Ticket> ongoingBillings = new HashMap<>();

    Map<ChargerType, Deque<ParkingLot>> availableParkingLots = new HashMap<>();

    /**
     * Parking without any space
     * @param policy palicy
     */
    public Parking(PricingPolicy policy) {
        this(policy, new HashMap<>());
    }

    /**
     * create parking
     *
     * @param policy     pricing policy
     * @param numberLots number of parking lots per charger type
     */
    public Parking(PricingPolicy policy, Map<ChargerType, Integer> numberLots) {
        this.policy = policy;

        int id = 0;
        for (ChargerType t : ChargerType.values()) {
            int num = numberLots.getOrDefault(t, 0);
            Deque<ParkingLot> queue = new LinkedList<>();
            for (int i = 0; i < num; i++) {
                queue.add(new ParkingLot(id++, t));
            }
            availableParkingLots.put(t, queue);
        }
    }

    /**
     * park a vehicle
     * @param vehicle vehicle to unpark
     * @param start start time
     * @return ticket
     */
    public synchronized Optional<Ticket> park(Vehicle vehicle, LocalDateTime start) {

        if (ongoingBillings.containsKey(vehicle)) {
            return Optional.of(ongoingBillings.get(vehicle));
        }

        Deque<ParkingLot> lots = this.availableParkingLots.get(vehicle.getChargerType());
        if (lots.isEmpty()) {
            log.info("No suitable parking lots found for vehicle {}", vehicle);
            return Optional.empty();
        }

        ParkingLot lot = lots.poll();
        Ticket ticket = new Ticket(vehicle, lot, start, policy);
        ongoingBillings.put(vehicle, ticket);
        log.info("issued ticket {}", ticket);
        return Optional.of(ticket);
    }

    /**
     * park a vehicle, synchronized to avoid race conditions
     * @param vehicle vehicle to park
     * @return generated partial ticket
     */
    public Optional<Ticket> park(Vehicle vehicle) {
        return this.park(vehicle, LocalDateTime.now());
    }

    /**
     * unpark a vehicle, synchronized to avoid race conditions
     * @param vehicle vehicle to unpark
     * @param end end time
     * @return ticket
     */
    public synchronized Optional<Ticket> unpark(Vehicle vehicle, LocalDateTime end) {
        if (!ongoingBillings.containsKey(vehicle)) {
            log.info("Vehicle {} not found in parking", vehicle);
            return Optional.empty();
        }

        Ticket ticket = ongoingBillings.remove(vehicle);
        ticket.setEnd(end);

        ParkingLot lot = ticket.getParkingLot();
        availableParkingLots.get(lot.getChargerType()).push(lot);

        log.info("unparked vehicle with ticket {}", ticket);
        return Optional.of(ticket);
    }

    /**
     * unpark a vehicle
     * @param vehicle vehicle to unpark
     * @return generated ticket
     */
    public Optional<Ticket> unpark(Vehicle vehicle) {
        return this.unpark(vehicle, LocalDateTime.now());
    }

}
