package org.szhao.parking;

import java.time.Duration;

/**
 * @author zsh
 * created 2020/Dec/05
 */
public interface PricingPolicy {
    /**
     * calculate price based on time
     * @param hours number of hours
     * @return price
     */
    double getPrice(Duration hours);
}
