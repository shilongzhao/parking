package org.szhao.parking;

import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;
import static org.szhao.parking.ChargerType.*;

/**
 * @author zsh
 * created 2020/Dec/05
 */
public class ParkingSpecTest {

    /**
     * test empty parking with no parking lots
     */
    @Test
    public void emptyParkingLotsParkTest() {
        PricingPolicy policy = new PerHour(2.0);
        Parking parking = new Parking(policy);
        Vehicle v = new Vehicle("EA123BC", KW20);
        assertEquals(Optional.empty(), parking.park(v));
    }

    /**
     * drain up parking lots
     */
    @Test
    public void singleParkingLotsParkTest() {
        Map<ChargerType, Integer> numLotsPerType = new HashMap<>();

        int N = 10;
        for (ChargerType tt: ChargerType.values()) {
            numLotsPerType.put(tt, N);
        }

        PricingPolicy policy = new PerHour(0.0);

        Parking parking = new Parking(policy, numLotsPerType);

        for (int i = 0; i < N; i++) {
            Vehicle vehicle = new Vehicle("v" + i, NONE);
            Optional<Ticket> billing = parking.park(vehicle);
            assertTrue(billing::isPresent);
        }
        assertTrue(parking.park(new Vehicle("v" + N, NONE)).isEmpty());
    }

    @Test
    public void noSuitableParkingLotTest() {
        PricingPolicy policy = new PerHour(2.0);
        Map<ChargerType, Integer> map = new HashMap<>();
        map.put(KW20, 1);
        Parking p = new Parking(policy, map);

        Vehicle v = new Vehicle("EM-123-NC", KW50);

        Optional<Ticket> t = p.park(v);
        assertTrue(t.isEmpty());
    }

    /**
     * test single vehicle park/unpark
     */
    @Test
    public void singleVehicleParkUnparkTest() {
        PricingPolicy policy = new PerHour(2.0);
        Map<ChargerType, Integer> map = new HashMap<>();
        map.put(KW50, 1);
        Parking p = new Parking(policy, map);

        Vehicle v = new Vehicle("EM-123-NC", KW50);
        LocalDateTime start = LocalDateTime.of(2020, 1, 1, 12, 1, 1);
        p.park(v, start);
        LocalDateTime end = LocalDateTime.of(2020, 1, 2, 13, 2, 2);
        Optional<Ticket> ticket = p.unpark(v, end);
        assertTrue(ticket.isPresent());
        Ticket t = ticket.get();
        assertEquals(v, t.getVehicle());
        assertEquals(policy, t.getPolicy());
        assertNotNull(t.getParkingLot());
        assertEquals(start, t.getStart());
        assertEquals(end, t.getEnd());
        assertEquals(policy.getPrice(Duration.between(start, end)), t.getPrice());
    }

    /**
     * Parking with multiple threads
     */
    @Test
    public void multiThreadParkingTest() {
        ExecutorService threadPool = Executors.newFixedThreadPool(8);
        PricingPolicy policy = new PerHour(2.2);
        Map<ChargerType, Integer> map = new HashMap<>();
        int N = 1000;
        map.put(KW20, N);
        Parking p = new Parking(policy, map);

        for (int i = 0; i < N - 1; i++) {
            Vehicle v = new Vehicle("v" + i, KW20);
            threadPool.submit(() -> p.park(v));
        }

        threadPool.shutdown();
        try {
            threadPool.awaitTermination(10, TimeUnit.SECONDS);
            Optional<Ticket> t = p.park(new Vehicle("v" + (N - 1), KW20));
            assertTrue(t.isPresent());
            t = p.park(new Vehicle("v" + N, KW20));
            assertTrue(t.isEmpty());
        } catch (InterruptedException e) {
            fail();
        }
    }


}
